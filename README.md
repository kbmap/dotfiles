# dotfiles

## Install

    git clone https://gitlab.com/coy/dotfiles.git ~/.dotfiles

    cat >> ~/.bashrc <<-EOF
    ####  BEGIN: DOTFILES  ####
    if [ -f ~/.dotfiles/bash_auto_load ]; then
        . ~/.dotfiles/bash_auto_load
    fi
    ####  END: DOTFILES  ####
    EOF

    cat >> ~/.bash_logout <<-EOF
    ####  BEGIN: DOTFILES  ####
    if [ -f ~/.dotfiles/bash_auto_unload ]; then
        . ~/.dotfiles/bash_auto_unload
    fi
    ####  END: DOTFILES  ####
    EOF

## Uninstall

    rm -rf ~/.dotfiles
    sed -i -n '/####  BEGIN: DOTFILES  ####/{:a; N; /####  END: DOTFILES  ####/!ba; d}; p' ~/.bashrc
    sed -i -n '/####  BEGIN: DOTFILES  ####/{:a; N; /####  END: DOTFILES  ####/!ba; d}; p' ~/.bash_logout
